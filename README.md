# csgo-cfg

* check out [settings.gg/iamf4bio](https://settings.gg/iamf4bio) for a more sexy looking view of my config

## how to

1. put all files in

    ```powershell
    %ProgramFiles(x86)%\Steam\userdata\<YOUR STEAM ID>\730\local\cfg\
    ```

2. adjust values to your liking - good starting point is probably [autoexec.cfg](./autoexec.cfg)

3. append following to csgo launch options (in steam: right-click &#8594; properties &#8594; set launch options...)

    ```powershell
    ... -exec autoexec ...
    ```

    example:

    ```powershell
    -novid -nojoy -high -nogammaramp -threads 4 -refresh 165 -exec autoexec.cfg
    ```

4. and/or run `exec autoexec` in-game via developer-console

## notes

* if you're wondering about that weird `zoom_sensitivity_ratio_mouse` value:
    > If you are a rifler and want to use the same relative sensitivity,while being scoped and unscoped use this:
    > zoom_sensitivity_ratio_mouse 0.818933027098955175

    Taken from [prosettings.net](https://prosettings.net/cs-go-best-settings-options-guide/) (thanks!)

* "casters crosshair" code: `CSGO-aNKFP-FzteR-6uRz5-4WP64-X6urD`
* my crosshair code: `CSGO-EBvtc-TKuWV-BFnvT-zrCF2-aSunC`
